import datetime

class Employee:

    _raise_amount = 1.1
    num_of_emps = 0

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.email = f"{self.first.lower()}.{self.last.lower()}@company.pl"
        self.pay = pay
        Employee.num_of_emps += 1

    def fullname(self):
        return f"{self.first} {self.last}"

    def apply_raise(self):
        self.pay = self.pay * self.raise_amount

    @classmethod
    def get_company_name(cls):
        print("Nazwa firmy")

    @classmethod
    def set_raise_amount(cls, amount):
        cls._raise_amount = amount

    @staticmethod
        def is_workingday(day):
            if  day.weekday() == 5 or day.weekday() == 6:
                 return False
             return True


class Developer(Employee):
    _raise_amount = 1.15
    def __init__(self, first, last, pay, prog_lang):
        super().__init__(first, last, pay)
        self.prog_lang = prog_lang

emp1 = Employee("Jan", "Kowalski", 100000)
emp2 = Employee("Ania", "Kwiatkowska", 100000)

dev1 = Developer("Jane", "Doe", 20000, "Python")
dev2 = Developer("Jan", "Soe", 6000, "C++")

print(dev1.prog_lang)


my_date = datetime.date(2019, 10, 11)
print (is_workingday((my_date))


